# Clustering

Using the concepts of stemming and collocations to cluster books from the Gutenberg collection   

The file `cluster.py` was written by me. The other files were written by [Dr. Ball](https://icarus.cs.weber.edu/~rball/)

## Running
To run the code use:
```sh
python3 cluster.py
```