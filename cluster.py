from nltk.stem import PorterStemmer
import matplotlib.pyplot as plt
import os
import re
import bigram_pmi as pmi
import nltk
import string
import csv
import kmeans_nlp
import hierarchical_nlp

#Give this variable which folder your books are in
dataPath = "./Subset3"
see_visuals = True
labels = []
authors = []
corpus = []
ps = PorterStemmer()

print("Getting books...")
books = os.listdir(dataPath)
bookText = []
for i, book in enumerate(books):
    print(f"Book {i} of {len(books)}", end="\r")
    #get authors name by getting the name that comes before the __
    authors.append(re.split('__',book)[0]) 
    labels.append(re.split('__',book)[1])
    all_words = ""
    with open(dataPath + "/" + book) as f:
        for line in f:
            line = line.lower()
            line = line.replace("he", "they")
            line = line.replace("she", "they")
            #porterStemmer each word in the line
            line = " ".join([ps.stem(i) for i in line.split()]) + " "
            all_words += line 

    bookText.append(all_words)
print()

print("Collocating...Filtering...Scoring...")
megabigrams = []
for i, book in enumerate(bookText):
    print(f"Book {i} of {len(bookText)}", end="\r")
    book = book.lower()
    sentences = nltk.sent_tokenize(book) #turns book into list of sentences 
    translator = str.maketrans('', '', string.punctuation) #removes punctuation
    sentences = [sentence.translate(translator) for sentence in sentences]
    
    tokenizedSentences = list(map(nltk.tokenize.word_tokenize, sentences))  # separates the txt into a list of words for each sentence

    bigrams = []
    for sentence in tokenizedSentences: #turns each sentince into a bigram
        bigrams.extend(list(nltk.ngrams(sentence, 2)))
    

    bigrams_fequency = {}
    for bigram in bigrams: #couts up the bigrams and puts them in a dictionary
        if bigram not in bigrams_fequency:
            bigrams_fequency[bigram] = 1
        else:
            bigrams_fequency[bigram] += 1
    
    filtered_bigrams = []
    #if bigram's frequency is less than 3 then remove
    for key in bigrams_fequency:
        if bigrams_fequency[key] >= 3:
            filtered_bigrams.append(key)
    
    unigrams = nltk.word_tokenize(book.translate(translator))
    # this gets the frequency of every unigram:
    unigrams_frequency = {}
    for u in unigrams:
        if u not in unigrams_frequency:
            unigrams_frequency[u] = 1
        else:
            unigrams_frequency[u] += 1
    
    bigram_scores = {}
    for b in filtered_bigrams:
        bigram_scores[b] = pmi.bigram_pmi(b, unigram_frequency=unigrams_frequency, bigram_frequency=bigrams_fequency)

    sortedList = sorted(bigram_scores, key=bigram_scores.get, reverse=True)
    sortedList = sortedList[:10]
    megabigrams.append(sortedList)
    

    topTEN = []
    for sl in sortedList:
        topTEN.append(bigrams_fequency[sl])
    corpus.append(topTEN)

    if len(topTEN) < 10:
        while len(topTEN) < 10:
            topTEN.append(0)
print("")


print("writing csv...")
pathtoTEMCSV = "./tempCSV.csv"
f = open(pathtoTEMCSV, 'w')
writer = csv.writer(f)
headerList = ['books']
for l in megabigrams:
    for bg in l:
        if bg not in headerList:
            t = bg[0] + " " + bg[1]
            headerList.append(t)
writer.writerow(headerList)
for i, bgList in enumerate(megabigrams):
    row = []
    for col in headerList:
        if col == 'books':
            row.append(authors[i]) #+":"+labels[i]
        else:
            t = col.split(" ")
            t = tuple(t)
            if t in bgList:
                row.append(corpus[i][bgList.index(t)])
            else:
                row.append(0)
    writer.writerow(row)
f.close()

kmeans_nlp.kmeans_drBallCode(pathtoTEMCSV)
hierarchical_nlp.hierarchial_drBall(pathtoTEMCSV)

os.remove(pathtoTEMCSV)#deletes file from directory

exit()
