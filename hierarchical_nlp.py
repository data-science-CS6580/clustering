import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from scipy.cluster.hierarchy import dendrogram, linkage

def hierarchial_drBall(path):
    df = pd.read_csv(path)

    # no width requirement:
    pd.options.display.width = 0

    features = list(df.columns[1:])
    features_df = df.loc[:, features]
    y = df['books']

    # It is important to scale the data so that weight does not have too much of an impact!!
    scaled_features = StandardScaler().fit_transform(features_df.values)

    # call the hierarchical aggregation clustering function:
    Z = linkage(scaled_features, 'ward')

    # visualize the results:
    dendrogram(Z, labels=y.values)
    plt.show()
