from sklearn.cluster import KMeans
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

def kmeans_drBallCode(path):
    df = pd.read_csv(path)
    label = 'books'

    do_pca = True
    # no width requirement:
    pd.options.display.width = 0

    features = list(df.columns[1:])
    features_df = df.loc[:, features]
    y = df[label]

    df_tr = pd.get_dummies(df, columns=[label])
    X = df_tr.values

    # Begin K-means code here:
    # It is important to scale the data so that weight does not have too much of an impact!!
    scaled_features = StandardScaler().fit_transform(df_tr.values)

    # Call the k-means function:
    for cluster in [2,5,10,25,50]:#range(2, 6):
        kmeans = KMeans(n_clusters=cluster, random_state=0)  # set 'random_state' to a seed to make the results deterministic
        kmeans.fit(scaled_features)

        labels = kmeans.labels_
        #print(labels)
        #print(f"\nK-means labels for {cluster} clusters: {kmeans.labels_}, which translates to the following clusters:")
        print(f"\tCLUSTER WITH K={cluster}\n")
        for my_cluster in range(cluster):
            print(f"\nClassifcation {my_cluster}:\n__________")
            for i, kmeans_label in enumerate(labels):
                if my_cluster == kmeans_label:
                    print(df[label][i])

        df_tr['clusters'] = labels

        # Show the visual results with the features reduced to only 2 components:
        # the following will actually scale all the values
        two_D_numpy_array = StandardScaler().fit_transform(features_df)

        # unfortunately, StandardScalar returns a 2D numpy array, not a dataframe
        # convert back to a dataframe:
        scaled_features_df = pd.DataFrame(data=two_D_numpy_array, columns=features)

        # print(scaled_features_df.head())
        # create the PCA object and set it so that it will reduce the features (columns) down to two components:
        pca = PCA(n_components=2)

        principal_components = pca.fit_transform(scaled_features_df)
        pca_result = pd.DataFrame(data=principal_components, columns=['principal_component_1', 'principal_component_2'])

        # we lost the class, so let's add that (concatenate the 'class' series) onto 'pca_result'
        finalDf = pd.concat([pca_result, df[[label]]], axis=1)

        # add the cluster to the dataframe
        finalDf['cluster'] = labels.tolist()

        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlabel('Principal Component 1', fontsize=15)
        ax.set_ylabel('Principal Component 2', fontsize=15)
        ax.set_title(f'2 component PCA for {cluster} clusters', fontsize=20)
        targets = y.tolist()

        # color stuff:
        color_labels = finalDf['cluster'].unique()
        # List of RGB triplets
        # you can pick your own color palette from here: http://seaborn.pydata.org/tutorial/color_palettes.html
        rgb_values = sns.color_palette("hls", n_colors=cluster)  # 'cluster' is the number of clusters
        # Map label to RGB
        color_map = dict(zip(color_labels, rgb_values))
        # print(f"The following is the color scheme for the clusters: {color_map}.")

        for index, row in finalDf.iterrows():
            ax.scatter(row['principal_component_1'], row['principal_component_2'], color=color_map[row['cluster']])
        ax.legend(targets)
        ax.grid()

        plt.show()
